CREATE TABLE `group_status` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `month` int(11) NOT NULL,
  `status` varchar(64) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `group_id` (`group_id`,`year`,`month`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `group2view` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `view_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `sites` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `grok_code` varchar(64) DEFAULT NULL,
  `server` varchar(64) DEFAULT NULL,
  `giu_code` varchar(64) DEFAULT NULL,
  `project` varchar(64) DEFAULT NULL,
  `language` varchar(64) DEFAULT NULL,
  `name` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `server` (`server`),
  UNIQUE KEY `giu_code` (`giu_code`),
  UNIQUE KEY `project` (`project`,`language`),
  KEY `grok_code` (`grok_code`)
) ENGINE=InnoDB AUTO_INCREMENT=774 DEFAULT CHARSET=utf8;

CREATE TABLE `views` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `site` int(11) NOT NULL,
  `title` varchar(255) NOT NULL DEFAULT '',
  `month` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `done` tinyint(1) NOT NULL DEFAULT '0',
  `namespace_id` int(11) NOT NULL,
  `page_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `site` (`site`,`page_id`,`month`,`year`)
) ENGINE=InnoDB AUTO_INCREMENT=1055301 DEFAULT CHARSET=utf8;

CREATE TABLE `groups` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `category` varchar(255) NOT NULL DEFAULT '',
  `depth` int(11) NOT NULL DEFAULT '5',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

