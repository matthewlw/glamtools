#!/usr/bin/php
<?PHP

# Re-calculates view data for a specific catalog, from a start year/month, 

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');

ini_set('memory_limit','7000M');

include_once ( '../public_html/php/common.php' ) ;

if ( count($argv) != 4 ) die ( "Params: start_year start_month catalog\n" ) ;

$year = $argv[1] * 1 ;
$month = $argv[2] * 1 ;
$catalog = $argv[3] * 1 ;

if ( $year*$month*$catalog == 0 ) die ( "No zeros!\n" ) ;

$db = openToolDB ( 'baglama2_p' ) ;
$db->set_charset("utf8") ;

while ( 1 ) {
	$cnt = 0 ;
	$sql = "SELECT count(*) AS cnt FROM group_status WHERE year=$year AND month=$month" ;
	$result = getSQL ( $db , $sql , 2 ) ;
	while($o = $result->fetch_object()) $cnt = $o->cnt ;
	if ( $cnt == 0 ) break ; // Done

	$cnt = 0 ;
	$sql = "SELECT * FROM group_status WHERE year=$year AND month=$month AND group_id=$catalog" ;
	$result = getSQL ( $db , $sql , 2 ) ;
	while($o = $result->fetch_object()) $cnt = 1 ;

	if ( $cnt == 0 ) { // Only if not exists
		exec ( "./add_month.php $year $month $catalog" ) ;
		exec ( "./load_viewdata.php $year $month $catalog" ) ;
	}
	
	$month++ ;
	if ( $month < 13 ) continue ;
	$year++ ;
	$month = 1 ;
}
