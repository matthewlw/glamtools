#!/usr/bin/php
<?php

require_once ( '/data/project/glamtools/baglama2/baglama.php' ) ;

if ( count($argv) < 3 ) {
	die ( "Usage: bg_create_catalog_month.php GROUP YEAR MONTH [COMMAND]\n" ) ;
}

$last_month = strtotime('last month') ;
if ( $argv[2] == 'lm' ) $argv[2] = date('Y',$last_month) ;
if ( $argv[3] == 'lm' ) $argv[3] = date('n',$last_month) ;

$group_id = $argv[1] ; // test:288 2018 2
$year = $argv[2] * 1 ;
$month = $argv[3] * 1 ;
$command = isset($argv[4]) ? strtoupper($argv[4]) : 'CREATE' ;

function createOrUpdateSqlite3 ( $group_id , $year , $month , $command ) {
	$overwrite = $command == 'CREATE' ;
	$bg = new Baglama ( 'baglama_sqlite3_creator_script' ) ;
	$ymk = $bg->getYearMonthKey ( $year , $month ) ;
	$fn_work = $bg->constructSqlite3TemporaryFilename ( $group_id , $ymk ) ;
	$fn_final = $bg->constructSqlite3Filename ( $group_id , $ymk ) ;

	if ( file_exists($fn_final) AND !file_exists($fn_work) ) {
		if ( $command == 'CREATE' ) unlink ( $fn_final ) ;
		else $fn_work = $fn_final ;
	}

	print "{$ymk}: {$group_id} [ {$fn_work} => {$fn_final} ]\n" ;

	$sqlite = $bg->openSqlite3File ( $fn_work , $overwrite ) ;
	if ( $command != 'VIEWS' ) {
		$bg->setGroupStatus ( $group_id , 0 , ['status'=>'GENERATING PAGE LIST','year'=>$year,'month'=>$month] ) ;
		$bg->seedSqlite3File ( $sqlite , $group_id , $year , $month ) ;
		$bg->addFilesToSqlite3 ( $sqlite , $group_id ) ;
		$bg->addPagesToSqlite3 ( $sqlite , $group_id , $year , $month ) ;
	}

	$bg->addViewCountsToSqlite3 ( $sqlite , $group_id , $year , $month ) ;
	$bg->finalizeSqlite3 ( $sqlite , $group_id , $fn_final ) ;

	$sqlite->close() ;
	if ( $fn_work != $fn_final ) {
		$bg->makeProductionDirectory ( $year , $month ) ;
		rename ( $fn_work , $fn_final ) ;
	}

}

if ( $group_id == 'all' ) {

	print "Starting all groups for {$year}-{$month}\n" ;
	$bg = new Baglama ( 'baglama_sqlite3_creator_script' ) ;
	$sql = "SELECT * FROM groups WHERE NOT EXISTS (SELECT * FROM group_status WHERE groups.id=group_id AND year={$year} AND month={$month})" ;
	$db = $bg->getToolDB() ;
	$result = $bg->tfc->getSQL ( $db , $sql ) ;
	while($o = $result->fetch_object()){
		$cmd = "jsub -mem 4g /data/project/glamtools/baglama2/bg_create_catalog_month.php {$o->id} {$year} {$month}" ;
		print "$cmd\n" ;
		passthru ( $cmd ) ;
	}
	$db->close() ;

} else if ( $group_id == 'just_added' ) {

	$bg = new Baglama ( 'baglama_sqlite3_creator_script' ) ;
	$sql = "SELECT * FROM groups WHERE just_added=1 AND NOT EXISTS (SELECT * FROM group_status WHERE groups.id=group_id AND year={$year} AND month={$month}) LIMIT 1" ;
	$db = $bg->getToolDB() ;
	$result = $bg->tfc->getSQL ( $db , $sql ) ;
	while($o = $result->fetch_object()) $group_id = $o->id ;

	if ( !isset($group_id) or $group_id=='just_added' ) exit(0);  #No just added

	# Set just_added to 0
	$sql = "UPDATE groups SET just_added=0 WHERE id={$group_id} AND just_added=1" ;
	$bg->tfc->getSQL ( $db , $sql ) ;

	$db->close() ;
	createOrUpdateSqlite3 ( $group_id , $year , $month , $command ) ;

} else if ( $group_id == 'next' ) {

	$bg = new Baglama ( 'baglama_sqlite3_creator_script' ) ;
	$sql = "SELECT * FROM groups WHERE NOT EXISTS (SELECT * FROM group_status WHERE groups.id=group_id AND year={$year} AND month={$month}) LIMIT 1" ;
	$db = $bg->getToolDB() ;
	$result = $bg->tfc->getSQL ( $db , $sql ) ;
	while($o = $result->fetch_object()) $group_id = $o->id ;
	$db->close() ;
	createOrUpdateSqlite3 ( $group_id , $year , $month , $command ) ;

} else if ( $group_id*1 > 0 ) {

	createOrUpdateSqlite3 ( $group_id , $year , $month , $command ) ;

}

?>