<?PHP

/*
error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');
*/
require_once ( "../php/common.php" ) ;
require_once ( '/data/project/glamtools/baglama2/baglama.php' ) ;


$action = get_request ( 'action' , '' ) ;
$bg = new Baglama ( 'baglama2_api' ) ;
$out = [ 'status' => 'OK' , 'data' => [] ] ;


if ( $action == 'overview' ) {

	$sql = "SELECT groups.id AS id,groups.category AS cat,max(year*100+month) AS date,count(*) AS cnt,sum(total_views) AS sum FROM groups,group_status WHERE groups.id=group_id AND status='VIEW DATA COMPLETE' GROUP BY groups.id ORDER BY groups.category" ;

	$result = $bg->getToolSQL ( $sql ) ;
	while($o = $result->fetch_object()){
		$out['data'][] = array ( 'cat' => $o->cat , 'gid' => $o->id , 'last' => $o->date , 'count' => $o->cnt , 'sum' => $o->sum ) ;
	}

} else if ( $action == 'add_category' ) {
	$category = trim ( get_request ( 'category' , '' ) ) ;
	$category = ucfirst ( trim ( str_replace ( '_' , ' ' , $category ) ) ) ;
	$depth = get_request ( 'depth' , '5' ) * 1 ;
	$user_name = get_request ( 'user_name' , '' ) ;
	$user_name = ucfirst ( trim ( str_replace ( '_' , ' ' , $user_name ) ) ) ;

	#$out['data'] = [ $category , $depth , $user_name ] ;

	if ( $category == '' ) {
		$out['status'] = 'ERROR: No category given' ;
	} else {
		$db = $bg->getToolDB() ;
		$sql = "SELECT * FROM `groups` WHERE `category`=\"".$db->real_escape_string($category)."\"" ;
		$result = $bg->getToolSQL ( $sql ) ;
		if ($o = $result->fetch_object()){
			$out['result'] = 'exists' ;
		} else {
			$sql = "INSERT IGNORE INTO `groups` (`category`,`depth`,`added_by`,`just_added`) VALUES (" ;
			$sql .= '"' . $db->real_escape_string ( $category ) . '",' . $depth . ',' ;
			$sql .= '"' . $db->real_escape_string ( $user_name ) . '",' ;
			$sql .= "1)" ;
			$out['sql'] = $sql ;
			$result = $bg->getToolSQL ( $sql ) ;
			$out['result'] = 'added' ;
		}
	}

} else if ( $action == 'monthly' ) {

	$gid = get_request ( 'gid' , 0 ) * 1 ;
	$sql = "SELECT * FROM group_status WHERE group_id=$gid ORDER BY year,month" ;
	$result = $bg->getToolSQL ( $sql ) ;
	while($o = $result->fetch_object()){
		$out['data'][$o->year][$o->month] = array ( 'status' => $o->status , 'views' => $o->total_views*1 ) ;
	}

} else if ( $action == 'month_overview' ) {

	$gid = get_request ( 'gid' , 0 ) * 1 ;
	$month = get_request ( 'month' , 0 ) * 1 ;
	$year = get_request ( 'year' , 0 ) * 1 ;
	
	$gsid = -1 ;
	$file = '' ;
	$sqlite_file = '' ;
	$sql = "SELECT * FROM group_status WHERE group_id=$gid AND year=$year AND month=$month" ;
	$result = $bg->getToolSQL ( $sql ) ;
	while($o = $result->fetch_object()){
		$gsid = $o->id ;
		$file = $o->file ;
		$sqlite_file = $o->sqlite3 ;
	}
	
	if ( $file == '' and $sqlite_file == '' ) { // DB
		$sql = "SELECT DISTINCT server,giu_code AS giu,name,language,project,pages,views FROM sites,gs2site,group_status WHERE site_id=sites.id AND group_status.id=group_status_id AND group_id=$gid AND year=$year and month=$month" ;
		$out['sql'] = $sql ;
		$result = $bg->getToolSQL ( $sql ) ;
		while($o = $result->fetch_object()){
			$o->pages *= 1 ;
			$o->views *= 1 ;
			$out['data'][] = (array) $o ;
		}

		$out['datasource'] = 'mysql' ;

	} else if ( $sqlite_file != '' ) {

		$sqlite = $bg->openSqlite3File ( $sqlite_file ) ;

		$sql = "SELECT DISTINCT server,giu_code AS giu,name,language,project,pages,views FROM sites,gs2site,group_status WHERE site_id=sites.id AND group_status.id=group_status_id" ; // "AND group_id=$gid AND year=$year and month=$month" superfluous
		$out['sql'] = $sql ;
		$result = $sqlite->query ( $sql ) ;
		while($o = $result->fetchArray(SQLITE3_ASSOC)) {
			$o['pages'] *= 1 ;
			$o['views'] *= 1 ;
			$out['data'][] = $o ;
		}

		$out['datasource'] = 'sqlite3' ;

	} else { // File
		$rows = explode ( "\n" , file_get_contents ( $file ) ) ;
		array_shift ( $rows ) ; // Header
		$d = [] ;
		foreach ( $rows AS $row ) {
			if ( trim($row) == '' ) continue ;
			$row = explode ( "\t" , $row ) ;
			$d[$row[0]][0] += $row[3]*1 ; // VIews
			$d[$row[0]][1] ++ ; // Pages
		}
		
		$sites = [] ;
		$sql = "SELECT * FROM sites" ;
		$result = $bg->getToolSQL ( $sql ) ;
		while($o = $result->fetch_object()){
			$sites[$o->giu_code] = $o ;
		}
		
		foreach ( $d AS $giu => $views ) {
			$out['data'][] = array (
				'server' => $sites[$giu]->server ,
				'giu' => $giu ,
				'name' => $sites[$giu]->name ,
				'language' => $sites[$giu]->language ,
				'project' => $sites[$giu]->project ,
				'pages' => $views[1] ,
				'views' => $views[0]
			) ;
		}

		$out['datasource'] = 'flatfile' ;
	}

	function cmp ( $a , $b ) {
		if ( $a['views'] == $b['views'] ) return 0 ;
		if ( $a['views'] < $b['views'] ) return 1 ;
		return -1 ;
	}
	
	usort($out['data'], "cmp");


	$format = get_request ( 'format' , 'json' ) ;
	if ( $format == 'tabbed' ) {
//		header('Content-type: text/plain');
		header('Content-type: application/octet-stream'); // text/plain
		header("Content-disposition: attachment; filename=baglama.monthly_overview.$gid.$year-$month.tab");
		print "Site\tPages\tViews\n" ;
		foreach ( $out['data'] AS $v ) {
			if ( $v['name'] == '' ) print $v['language'] . "." . $v['project'] ;
			else print $v['name'] . " " . ucfirst ( $v['project'] ) ;
			print "\t" . $v['pages'] . "\t" . $v['views'] . "\n" ;
		}
		exit ( 0 ) ;
	}

} else if ( $action == 'month_site' ) {

	$gid = get_request ( 'gid' , 0 ) * 1 ;
	$month = get_request ( 'month' , 0 ) * 1 ;
	$year = get_request ( 'year' , 0 ) * 1 ;
	$giu = get_request ( 'giu' , 0 ) ;
	$max = get_request ( 'max' , 0 ) * 1 ;

	$gsid = -1 ;
	$file = '' ;
	$sqlite_file = '' ;
	$sql = "SELECT * FROM group_status WHERE group_id=$gid AND year=$year AND month=$month" ;
	$result = $bg->getToolSQL ( $sql ) ;
	while($o = $result->fetch_object()){
		$gsid = $o->id ;
		$file = $o->file ;
		$sqlite_file = $o->sqlite3 ;
	}
	
	if ( $file == '' and $sqlite_file == '' ) { // DB
		$site = 0 ;

		$wiki = $giu ; // Yeah...
		$server = $bg->tfc->getWebserverForWiki ( $wiki ) ;

		$sql = "SELECT * FROM sites where server='{$server}'" ;
		$result = $bg->getToolSQL ( $sql ) ;
		while($o = $result->fetch_object()){
			$site = $o->id ;
		}
		
		$sql = "SELECT title,namespace_id,views,group_concat(DISTINCT image separator '|') AS files from views,group2view WHERE group2view.view_id=views.id AND site=$site AND done=1 AND group_status_id=$gsid GROUP BY view_id ORDER BY views DESC" ;
		if ( $max > 0 ) $sql .= " LIMIT $max" ;
		$result = $bg->getToolSQL ( $sql ) ;
		while($o = $result->fetch_object()){
			$o->views *= 1 ;
			$o->files = explode ( '|' , $o->files ) ;
			$out['data'][] = (array) $o ;
		}

		$out['datasource'] = 'mysql' ;

	} else if ( $sqlite_file != '' ) { // Sqlite3

		$sqlite = $bg->openSqlite3File ( $sqlite_file ) ;

		$wiki = $giu ; // Yeah...
		$server = $bg->tfc->getWebserverForWiki ( $wiki ) ;
		$out['server'] = $server ;
		$sql = "SELECT id FROM sites where server='{$server}' LIMIT 1" ;
		$site = $sqlite->querySingle ( $sql ) ;
		$out['site_id'] = $site ;

		$sql = "SELECT title,namespace_id,views,(SELECT group_concat(image,'|') FROM group2view WHERE group2view.view_id=views.id) AS files from views WHERE site={$site} AND done=1 ORDER BY views DESC" ; // 'AND group_status_id=$gsid' superfluous
		if ( $max > 0 ) $sql .= " LIMIT $max" ;
		$result = $sqlite->query ( $sql ) ;
		while($o = $result->fetchArray(SQLITE3_ASSOC)) {
			$o['views'] *= 1 ;
			$o['files'] = array_unique ( explode ( '|' , $o['files'] ) ) ;
			$out['data'][] = $o ;
		}
		$out['datasource'] = 'sqlite3' ;


	} else { // File (ancient format)

		$rows = explode ( "\n" , file_get_contents ( $file ) ) ;
		array_shift ( $rows ) ; // Header
		foreach ( $rows AS $row ) {
			if ( trim ( $row ) == '' ) continue ;
			$row = explode ( "\t" , $row ) ;
			if ( $row[0] != $giu ) continue ;
			$o = array (
				'title' => (str_replace('_',' ',urldecode($row[1]))) ,
				'namespace_id' => 0 ,
				'views' => $row[3]*1 ,
				'files' => explode ( '|' , ($row[2]) )
			) ;
			$out['data'][] = $o ;
		}
		
		function cmp ( $a , $b ) {
			if ( $a['views'] == $b['views'] ) return 0 ;
			if ( $a['views'] < $b['views'] ) return 1 ;
			return -1 ;
		}
		
		usort($out['data'], "cmp");
		if ( $max > 0 ) {
			$out['data'] = array_slice ( $out['data'] , 0 , 100 ) ;
		}

		$out['datasource'] = 'flatfile' ;

	}
	
	$format = get_request ( 'format' , 'json' ) ;
	if ( $format == 'tabbed' ) {
//		header('Content-type: text/plain');
		header('Content-type: application/octet-stream'); // text/plain
		header("Content-disposition: attachment; filename=baglama.month_site.$gid.$year-$month.$giu.tab");
		print "Title\tViews\tFile(s)\n" ;
		foreach ( $out['data'] AS $v ) {
			print $v['title'] . "\t" . $v['views'] . "\t" . implode('|',$v['files']) . "\n" ;
		}
		exit ( 0 ) ;
	}

} else {
	require_once '/data/project/magnustools/public_html/php/Widar.php' ;
	$widar = new \Widar ( 'glamtools' ) ;
	$widar->attempt_verification_auto_forward ( '/baglama2' ) ;
	$widar->authorization_callback = 'https://glamtools.toolforge.org/baglama2/api.php' ;
	if ( $widar->render_reponse(true) ) exit(0);

	$out['status'] = "UNKNOWN ACTION '{$action}'" ;
}

/*
if ( isset($_REQUEST['test']) ) {
	$x = json_encode ( $out ) ;
	header('Content-type: text/html');
	print "<pre>" ;
	print var_dump($x) ;
	exit ( 0 ) ;
}
*/

//print_r ( $out ) ; exit(0);

header('Content-type: application/json');
print json_encode ( $out ) ;
//if ( isset ( $_REQUEST['testing'] ) ) { print get_common_header() ."$sql" ; exit ( 0 ) ; }

exit ( 0 ) ;

?>
