<?PHP

if ( 1 ) {
	error_reporting(0);
} else {
	error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
	ini_set('display_errors', 'On');
}
ini_set('memory_limit','500M');

include_once ( "php/common.php" ) ;

$maxint = 2147483648 ; // 2**31

$basedir = '//data/project/glamtools/viewdata' ;
$main_stats_file = "$basedir/stats.total" ;

function prettynum ( $n , $show_plus = false ) {
	global $maxint ;
	$ret = number_format ( ( trim ( $n ) * 1 ) % $maxint ) ;
	if ( $show_plus and $ret > 0 ) $ret = "+$ret" ;
	return $ret ;
}

function get_page_link ( $project , $page ) {
	if ( substr ( $project , -4 , 4 ) == 'wiki' ) {
		$l = substr ( $project , 0 , -4 ) ;
		return "<a href=\"http://$l.wikipedia.org/wiki/" . myurlencode ( $page ) . "\">$page</a>" ;
	} else return $page ;
}


function get_project_data ( $date ) {
	global $group , $basedir , $maxint ;
	$group = preg_replace ( '|^[/.]+|' , '' , $group ) ; // Paranoia
	$h = fopen ( "$basedir/$date/$group.$date.tab" , "r" ) ;
	if ( !$h ) {
		print "Could not open file for $group / $date" ;
		exit ( 0 ) ;
	}
	$s = trim ( fgets ( $h ) ) ; // Header
	$files = array () ;
	$total = array () ;
	$days = array () ;
	$daycnt = array () ;
	$pagecnt = array () ;
	$tc = 0 ;
	while ( !feof ( $h ) ) {
		$s = trim ( fgets ( $h ) ) ;
		if ( $s == '' ) continue ;
		$s = explode ( "\t" , $s ) ;
		$project = array_shift ( $s ) ;
		$page = array_shift ( $s ) ;
		$page = str_replace ( '_' , ' ' , urldecode ( $page ) ) ;
		$files[$project][$page] = explode ( '|' , array_shift ( $s ) ) ;
		$tcnt = array_shift ( $s ) ;
		if ( $tcnt == '' ) { // Some odd bug
			$tcnt = 0 ;
			foreach ( $s AS $s2 ) $tcnt += $s2 ;
		}
		$tcnt = $tcnt % $maxint ;
		$total[$project] += $tcnt ;
		$pagecnt[$project][$page] = $tcnt ;
		$tc += $tcnt ;
		
		
		foreach ( $s AS $day => $cnt ) $daycnt[$day] += $cnt % $maxint ;
	}
	fclose ( $h ) ;
	
	$ret = array() ;
	foreach ( $total AS $k => $v ) {
		$ret[$k]['views'] = $v ;
		$ret[$k]['pages'] = count ( $pagecnt[$k] ) ;
	}
	
	return $ret ;
}

//________________________________________________________________________________________

header('Content-type: text/html');
header("Cache-Control: no-cache, must-revalidate");
print get_common_header ( "baglama.php" , "BaGLAMa" , array (
	'style' => 'td.num { text-align:right ; font-family:courier; font-size:9pt; }' ,
	'script' => "function toggle ( id ) { d=document.getElementById('details_'+id); d.style.display = d.style.display=='none'?'':'none'; return false;}"
) ) ;

print "<div class='container'>" ;
print "<div class='alert alert-danger'><b>This tool is deprecated!</b> Please use its successor, <a href='./baglama2/'>BaGLAMa 2</a>.</div>" ;
print "</div>" ;

print "<div class='span12' style='margin-bottom:10px'>" ;
print "<a href='http://en.wikipedia.org/wiki/Baglama'><img title='Baglama' style='float:right' src='http://upload.wikimedia.org/wikipedia/commons/0/05/Baglama_turc_manche_long.jpg' border='0' width='200px' height='132px' /></a>" ;

$group = get_request ( 'group' , '' ) ;
$date = get_request ( 'date' , '' ) ;
$diff = get_request ( 'diff' , '' ) ;
$max_details = 100 ;

$ym = array() ;
$main_stats = array () ;
$main_stats_tmp = explode ( "\n" , file_get_contents ( $main_stats_file ) ) ;
foreach ( $main_stats_tmp AS $row ) {
	if ( $row == '' ) continue ;
	$row = explode ( "\t", $row ) ;
	$main_stats[$row[0]][$row[1]] = array ( $row[2] , $row[3] ) ; // Cat, date = ( total_count , file )
	$ym[$row[1]] = '?' ;
}
ksort ( $main_stats ) ;
foreach ( $main_stats AS $g => $d ) {
	ksort (  $main_stats[$g] ) ;
}

function get_chart ( $group , $w , $h , $highlight = '' , $diff_highlight = '' ) {
	global $ym , $main_stats ;
	
	// Fill in dates
	$max = 0 ;
	$data = $ym ;
	$median = array() ;
	foreach ( $main_stats[$group] AS $date => $d ) {
		$v = $d[0] ;
		$data[$date] = $v ;
		if ( $max < $v ) $max = $v ;
		if ( $v > 0 ) $median[] = $v ;
	}
	
	sort ( $median ) ;
	$median = $median[floor(count($median)*3/4)] ; // Not a real median...
	$upper_limit = $median * 5 ;
	if ( $max > $upper_limit ) {
		$max = 0 ;
		foreach ( $data AS $date => $count ) {
			if ( $count > 0 and $count < $upper_limit && $count > $max ) $max = $count ;
		}
//		$max = $upper_limit ;
//		print prettynum($max) . " / " . prettynum($upper_limit) . "<br/>" ;
	}

	// Generate HTML
	$ret = '' ;
	$difflinks = '' ;
	$has_delta = false ;
	$lastyear = '' ;
	ksort ( $data ) ;
	foreach ( $data AS $date => $count ) {
		$col = '#EEEEEE' ; // Unknown
		$dh = $h ; // Full height
		$t = substr ( $date , 0 , 4 ) . "-" . substr ( $date , 4 , 2 ) . " : " ;
		if ( $count == '?' ) {
			$t .= "missing datapoint" ;
		} else {
			$col = '#62A9FF' ;
			$dh = floor ( $h * $count / $max ) ;
			$t .= prettynum ( $count ) . " page views" ;
			if ( $count > $upper_limit ) {
				$col = 'yellow' ;
				$t .= "; vastly exeeds most other values, probably on some Main Page this month, therefore bar is not to scale" ;
				$count = $max ;
				$dh = floor ( $h * $count / $max ) ;
			}
			if ( $date == $highlight or $date == $diff_highlight ) $col = '#FF4848' ;
		}
		if ( $dh == 0 ) $dh = 1 ;
		$diff = $h - $dh ;
		if ( $lastyear != '' and $lastyear != substr ( $date , 0 , 4 ) ) {
			$ret .= "<td><div style='height:1px;width:" . floor($w/2) . "px;'></div></td>" ;
			$difflinks .= "<td/>" ;
		}
		if ( $date != $highlight ) {
			$url = "?group=".urlencode($group)."&date=$date&diff=$highlight" ;
			if ( $count == '?' ) $difflinks .= "<td/>" ;
			else {
				$difflinks .= "<td style='text-align:center'><a style='text-decoration: none' href='$url'>&Delta;</a></td>" ;
				$has_delta = true ;
			}
		} else $difflinks .= "<td/>" ;
		$ret .= "<td valign='bottom'>" ;
		if ( $count != '?' ) $ret .= "<a href='./baglama.php?group=".urlencode($group)."&date=$date'>" ;
		$ret .= "<div class='statsbar' title='$t' data-toggle='tooltip'>" ;
		$ret .= "<div style='height:{$dh}px;background-color:$col;width:{$w}px;margin-right:1px;'></div>" ;
		if ( $count != '?' ) $ret .= "</a>" ;
		$ret .= "</div>" ;
		$ret .= "</td>" ;
		$lastyear = substr ( $date , 0 , 4 ) ;
	}

	if ( $highlight != '' and $diff_highlight == '' and $has_delta ) $ret .= "<tr>" . $difflinks . "<td><small>(click on a &Delta; for a diff to this data)</small></td></tr>" ;

	$ret = "<table border=0 cellspacing=0 cellpadding=0 style='font-size:9pt'><tr>" . $ret . "</tr></table>" ;


	return $ret ;
}

if ( $group == '' or $date == '' ) {
	$fg = array () ;
	foreach ( $main_stats AS $group => $d1 ) {
		foreach ( $d1 AS $date => $d2 ) {
			$fg[$group][$date] = $d2[1] ;
		}
	}

	print "<p>View counts for pages using Commons images in GLAM-related category trees.</p>" ;
	print "<p><i>Note:</i> Updates are slow at the moment; once view data is available on this system, <br/>updates will be quicker and more regular. Patience.</p>" ;
	print "</div><div class='span12'>" ;

	
	$w = "style='width:20px'" ;
	print "<table border=1 cellspacing=0 cellpadding=2>" ;
	foreach ( $fg AS $group => $dates ) {
		$years = array() ;
		foreach ( $dates AS $d => $f ) {
			$y = substr ( $d , 0 , 4 ) ;
			$m = substr ( $d , 4 , 2 ) ;
			$years[$y][$m] = $f ;
		}
		
		$out = '<table>' ;
		ksort ( $years ) ;
		foreach ( $years AS $y => $d1 ) {
			$out .= "<tr><th style='padding-right:10px'>$y</th>" ;
			for ( $m = 1 ; $m <= 12 ; $m++ ) {
				$m2 = $m ;
				if ( $m < 10 ) $m2 = "0$m" ;
				if ( isset ( $years[$y][$m2] ) ) {
					$out .= "<td $w><a href='./baglama.php?group=" . urlencode($group) . "&date=$y$m2'>$m2</a></td>" ;
				} else {
					$out .= "<td $w>&ndash;</td>" ;
				}
			}
			$out .= "</tr>" ;
		}
		$out .= "</table>" ;
		
		$g2 = str_replace ( '_' , ' ' , $group ) ;
		print "<tr><td>" ;
		print "<a href='#" . urlencode ( $group ) . "' name='" . urlencode ( $group ) . "'>#</a>&nbsp;" ;
		print "<a href='http://commons.wikimedia.org/wiki/Category:".myurlencode($group)."'>$g2</a></td><td>" . $out . "</td>" ;
		print "<td nowrap>" . get_chart ( $group , 8 , 50 ) . "</td>" ;
		print "</tr>" ;
	}
	print "</table>" ;
} else if ( $diff != '' ) {
	if ( $diff > $date ) { $x = $date ; $date = $diff ; $diff = $x ; } // $diff needs to be the first date!

	$t1 = substr ( $diff , 0 , 4 ) . '-' . substr ( $diff , 4 , 2 ) ;
	$t2 = substr ( $date , 0 , 4 ) . '-' . substr ( $date , 4 , 2 ) ;

	$d1 = get_project_data ( $diff ) ;
	$d2 = get_project_data ( $date ) ;
	
	$projects = array() ;
	foreach ( $d1 AS $k => $v ) $projects[$k][0] = $v ;
	foreach ( $d2 AS $k => $v ) $projects[$k][1] = $v ;
	
	$pk = array() ;
	foreach ( $projects AS $k => $v ) {
		if ( !isset($v[0]) ) { $v[0]['views'] = 0 ; $v[0]['pages'] = 0 ; }
		if ( !isset($v[1]) ) { $v[1]['views'] = 0 ; $v[1]['pages'] = 0 ; }
		$pk[$k] = $v[0]['views'] + $v[1]['views'] ;
	}
	arsort ( $pk ) ;
	
//	var_dump ( $d1 ) ; print "<hr/>" ; var_dump ( $d2 ) ;
	
	$total = array() ;
	print "<h2>Difference of page views of pages containing files from \"$group\" between $t1 and $t2</h2>" ;
	print "<p>(Article namespace only)</p>" ;
	print "</div><div class='span12'>" ;
	print "<div style='padding:5px'>" ;
	print get_chart ( $group , 16 , 50 , $date , $diff ) ;
	print "</div>" ;
	print "<table border='1' cellspacing='0' cellpadding='2'>" ;
	print "<thead><tr><th rowspan=2>Project</th><th colspan=2>$t1</th><th colspan=2>$t2</th><th colspan=2>$t2 &ndash; $t1</th></tr>" ;
	print "<tr><th>Page hits</th><th>Pages</th><th>Page hits</th><th>Pages</th><th>&Delta;Page hits</th><th>&Delta;Pages</th></tr></thead><tbody>" ;
	foreach ( $pk AS $project => $dummy ) {
		print "<tr><th>$project</th>" ;
		print "<td class='num'>" . prettynum ( $projects[$project][0]['views'] ) . "</td>" ;
		print "<td class='num'>" . prettynum ( $projects[$project][0]['pages'] ) . "</td>" ;
		print "<td class='num'>" . prettynum ( $projects[$project][1]['views'] ) . "</td>" ;
		print "<td class='num'>" . prettynum ( $projects[$project][1]['pages'] ) . "</td>" ;
		
		$dv = $projects[$project][1]['views'] - $projects[$project][0]['views'] ; // Diff views
		$dp = $projects[$project][1]['pages'] - $projects[$project][0]['pages'] ; // Diff pages
		
		$cvd = $dv < 0 ? 'red' : ( $dv > 0 ? 'green' : 'black' ) ; // Color views diff
		$cpd = $dp < 0 ? 'red' : ( $dp > 0 ? 'green' : 'black' ) ; // Color pages diff

		print "<td class='num' style='color:$cvd'>" . prettynum ( $dv , true ) . "</td>" ;
		print "<td class='num' style='color:$cpd'>" . prettynum ( $dp , true ) . "</td>" ;
		
		print "</tr>" ;

		$total[0] += $projects[$project][0]['views'] ;
		$total[1] += $projects[$project][0]['pages'] ;
		$total[2] += $projects[$project][1]['views'] ;
		$total[3] += $projects[$project][1]['pages'] ;
		$total[4] += $dv ;
		$total[5] += $dp ;

	}
	print "</tbody><tfoot><tr><td colspan=7></td></tr><tr><th>Total</th>" ;

	print "<td class='num'>" . prettynum ( $total[0] ) . "</td>" ;
	print "<td class='num'>" . prettynum ( $total[1] ) . "</td>" ;
	print "<td class='num'>" . prettynum ( $total[2] ) . "</td>" ;
	print "<td class='num'>" . prettynum ( $total[3] ) . "</td>" ;

	$cvd = $total[4] < 0 ? 'red' : ( $total[4] > 0 ? 'green' : 'black' ) ; // Color views diff
	$cpd = $total[5] < 0 ? 'red' : ( $total[5] > 0 ? 'green' : 'black' ) ; // Color pages diff
	print "<td class='num' style='color:$cvd'>" . prettynum ( $total[4] , true ) . "</td>" ;
	print "<td class='num' style='color:$cpd'>" . prettynum ( $total[5] , true ) . "</td>" ;

	print "</tr></tfoot>" ;
	print "</table>" ;
	
	
	
} else {
	$d2 = substr ( $date , 0 , 4 ) . '-' . substr ( $date , 4 , 2 ) ;
	print "<h2>Page views of pages containing files from \"$group\" for $d2</h2>" ;
	print "<p>(Article namespace only)</p>" ;
	$group = preg_replace ( '|^[/.]+|' , '' , $group ) ; // Paranoia
	$h = fopen ( "$basedir/$date/$group.$date.tab" , "r" ) ;
	if ( !$h ) {
		print "Could not open file for $group / $date" ;
		exit ( 0 ) ;
	}
	$s = trim ( fgets ( $h ) ) ; // Header
	$files = array () ;
	$total = array () ;
	$days = array () ;
	$daycnt = array () ;
	$pagecnt = array () ;
	$tc = 0 ;
	while ( !feof ( $h ) ) {
		$s = trim ( fgets ( $h ) ) ;
		if ( $s == '' ) continue ;
		$s = explode ( "\t" , $s ) ;
		$project = array_shift ( $s ) ;
		$page = array_shift ( $s ) ;
		$page = str_replace ( '_' , ' ' , urldecode ( $page ) ) ;
		$files[$project][$page] = explode ( '|' , array_shift ( $s ) ) ;
		$tcnt = array_shift ( $s ) ;
		if ( $tcnt == '' ) { // Some odd bug
			$tcnt = 0 ;
			foreach ( $s AS $s2 ) $tcnt += $s2 ;
		}
		if ( !isset ( $total[$project] ) ) $total[$project] = 0 ;
		$total[$project] += $tcnt ;
		$pagecnt[$project][$page] = $tcnt ;
		$tc += $tcnt ;
		
		
		foreach ( $s AS $day => $cnt ) {
			if ( !isset($daycnt[$day]) ) $daycnt[$day] = 0 ;
			$daycnt[$day] += $cnt ;
		}
		
//		$days[$project][$file] = $s ;
//		print "$s</br>" ;
	}
	fclose ( $h ) ;
	
	arsort ( $total ) ;
	print "<table width='100%' border='1'><tbody>" ;
	print "<tr><th style='background:#DDDDDD' nowrap>Total views<br/>in $d2</th><td nowrap style='background:#DDDDDD'>" . prettynum($tc) . "</td><td>" ;
	print get_chart ( $group , 16 , 50 , $date ) ;
	print "</td></tr>" ;
	$idc = 0 ;
	foreach ( $total AS $project => $count ) {
		if ( $count == 0 ) continue ;
		print "<tr><th valign='top'>$project</th><td valign='top' style='text-align:right' nowrap>" . prettynum($count) . "</td><td style='width:100%'>" ;
		
		$idc++ ;
		print "<a href='#' onclick='toggle($idc);return false'>Show/hide details</a> of " . count ( $pagecnt[$project] ) . " pages<br/>" ;
		
		arsort ( $pagecnt[$project] ) ;
		
		$msg = '' ;
		if ( count ( $pagecnt[$project] ) > $max_details ) $msg = "<tr><td colspan='3'><i>Only the top $max_details pages are shown</i></td></tr>" ;
		
		print "<table border='1' id='details_$idc' style='display:none'><tr><th>Page</th><th>Views</th><th>Files from \"$group\" used on page</th></tr>" ;
		print $msg ;
		$rowcnt = 0 ;
		foreach ( $pagecnt[$project] AS $page => $cnt ) {
			if ( $cnt == 0 ) continue ;
			$rowcnt++ ;
			if ( $rowcnt > $max_details ) break ;
			$pl = get_page_link ( $project , $page ) ;
			$a = array () ;
			foreach ( $files[$project][$page] AS $i ) $a[] = "<a href=\"http://commons.wikimedia.org/wiki/File:$i\">$i</a>" ;
			print "<tr><td>$pl</td><td nowrap style='text-align:right'>" . prettynum($cnt) . "</td><td>" . count ( $a ) . " (<small>" . implode ( '; ' , $a ) . "</small>)</td></tr>" ;
		}
		print "</table>" ;
		
		print "</td></tr>" ;
	}
	print "</tbody></table><hr/>" ;
	
	$th = '' ;
	$td = '' ;
	$tc = 0 ;
	foreach ( $daycnt AS $day => $cnt ) {
		$th .= "<th>" . ( $day + 1 ) . "</th>" ;
		$td .= "<td>$cnt</td>" ;
		$tc += $cnt ;
	}
	$th .= "<th>&Sigma;</th>" ;
	$td .= "<td>$tc</td>" ;
	print "<table border='1' style='font-size:7pt'><thead>Total page views per day</thead><tbody>" ;
	print "<tr>$th</tr>" ;
	print "<tr>$td</tr>" ;
	print "</tbody></table>" ;
}

print "</div>" ;
print "<script>$('div.statsbar').tooltip({placement:'bottom'});</script>" ;

print get_common_footer() ;

# Logging
require_once ( "php/ToolforgeCommon.php" ) ;
$tfc = new ToolforgeCommon('baglama1') ;
$tfc->logToolUse('','load') ;

?>