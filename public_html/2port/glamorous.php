<?PHP

if ( isset ( $_REQUEST['format'] ) ) $format = $_REQUEST['format'] ;
else $format = '' ;

if ( $format == 'xml' ) {
	$hide_header = true ;
	$hide_doctype = true ;
	header('Content-type: text/xml; charset=utf-8');
	print '<?xml version="1.0"?>' ;
}

include "common.php" ;
include "php/legacy.php" ;

$images_from_galleries = array () ;
$images_from_user = array () ;

function get_image_gallery_count ( $galleries ) {
	global $images_from_galleries ;
	$gp = explode ( "\n" , $galleries ) ;
	$q = new WikiQuery ( 'commons' ) ;
	$images_from_galleries = array () ;
	foreach ( $gp AS $g ) {
		$g = trim ( $g ) ;
		if ( $g == '' ) continue ;
		$a = $q->get_images_on_page ( $g ) ;
		foreach ( $a AS $b ) {
			$images_from_galleries[$b] = $b ;
		}
	}
	return count ( $images_from_galleries ) ;
}

function get_image_user_count ( $username ) {
	global $images_from_user ;
	$images_from_user = db_get_user_images ( $username , "commons" , "wikimedia" ) ;
	$images_from_user = array_keys ( $images_from_user ) ;
	return count ( $images_from_user ) ;
}

function get_image_count ( $category , $depth ) {
	global $gallerypages , $username ;
	if ( $username != '' ) return get_image_user_count ( $username ) ;
	if ( $gallerypages != '' ) return get_image_gallery_count ( $gallerypages ) ;
	$i = db_get_images_in_category ( 'commons' , $category , $depth , 'wikimedia' ) ;
	return count ( $i ) ;
}

function get_image_usage_count ( $language , $project , $category ) {
	global $ns0 , $show_details , $usage , $usecount ;
	$c2 = get_db_safe ( $category ) ;
	$mysql_con = db_get_con_new ( $language , $project ) ;
	$db = get_db_name ( $language , $project ) ;
	
	$ret = array () ;
	$sel = $show_details ? "p.page_title AS title,p.page_namespace AS ns,il_to" : "count(distinct il_to) as cnt" ;
	
	if ( $ns0 ) {
		$sql = "SELECT /* SLOW_OK */ /* GLAMOROUS */ $sel from page AS p,imagelinks,commonswiki_p.image ci,commonswiki_p.categorylinks cl,commonswiki_p.page cp where il_from=p.page_id and p.page_namespace=0 and ci.img_name=il_to and ci.img_name=cp.page_title and cp.page_namespace=6 and cp.page_id=cl.cl_from and cl.cl_to=\"$c2\"" ;
	} else {
		$sql = "SELECT /* SLOW_OK */ /* GLAMOROUS */ $sel from imagelinks,commonswiki_p.image ci,commonswiki_p.categorylinks cl,commonswiki_p.page cp where ci.img_name=il_to and ci.img_name=cp.page_title and cp.page_namespace=6 and cp.page_id=cl.cl_from and cl.cl_to=\"$c2\"" ;
	}
	$res = my_mysql_db_query ( $db , $sql , $mysql_con ) ;
	
	if ( $show_details ) {
		$cnt = 0 ;
		while ( $o = mysql_fetch_object ( $res ) ) {
			$usage[$o->il_to]["$language.$project"][$o->ns][$o->title] = $o->title ;
			$usecount[$o->il_to]++ ;
			$cnt++ ;
		}
		return $cnt ;
	} else {
		while ( $o = mysql_fetch_object ( $res ) ) {
			return $o->cnt ;
		}
	}
}

function run_globalusage_galleries ( $gallerypages ) {
	global $images_from_galleries ;
	global $ns0 , $show_details , $usage , $usecount , $project_usage ;

	$language = 'commons' ;
	$project = 'wikimedia' ;
	$mysql_con = db_get_con_new ( $language , $project ) ;
	$db = get_db_name ( $language , $project ) ;
	$cnt = 0 ;

	while ( count ( $images_from_galleries ) > 0 ) {
		$q = array () ;
		while ( count ( $images_from_galleries ) > 0 and count ( $q ) < 100 ) {
			$f = array_pop ( $images_from_galleries ) ;
			$f = explode ( ':' , $f , 2 ) ;
			$q[] = get_db_safe ( $f[1] ) ;
		}

		$q = '("' . implode ( '","' , $q ) . '")' ;
//		print "$q<br/>" ;
		$sql = "SELECT /* SLOW_OK */ /* GLAMOROUS */ gil_wiki,gil_page_title,gil_page_namespace,gil_to FROM commonswiki_p.globalimagelinks where gil_to IN $q" ;
		if ( $ns0 ) $sql .= ' AND gil_page_namespace=""' ;
		
//		print $sql ;

		$res = my_mysql_db_query ( $dbu , $sql , $mysql_con ) ;

		while ( $o = mysql_fetch_object ( $res ) ) {
			$a = array () ;
			if ( preg_match ( '/^(.+)(wik.+)$/' , $o->gil_wiki , $a ) ) {
				if ( $a[2] == 'wiki' ) $a[2] = 'wikipedia' ;
				$lp = $a[1] . '.' . $a[2] ;
			} else {
				$lp = '!!!' . $o->gil_wiki ;
			}
			$project_usage[$lp]++ ;
			$usage[$o->gil_to][$lp][$o->gil_page_namespace][$o->gil_page_title] = $o->gil_page_title ;
			$usecount[$o->gil_to]++ ;
			$cnt++ ;
		}
	}

	return $cnt ;
}

function run_globalusage_username ( $username ) {
	global $images_from_user ;
	global $ns0 , $show_details , $usage , $usecount , $project_usage ;

	$language = 'commons' ;
	$project = 'wikimedia' ;
	$mysql_con = db_get_con_new ( $language , $project ) ;
	$db = get_db_name ( $language , $project ) ;
	$cnt = 0 ;

	while ( count ( $images_from_user ) > 0 ) {
		$q = array () ;
		while ( count ( $images_from_user ) > 0 and count ( $q ) < 100 ) {
			$f = array_pop ( $images_from_user ) ;
//			$f = explode ( ':' , $f , 2 ) ;
			$q[] = get_db_safe ( $f ) ;
		}

		$q = '("' . implode ( '","' , $q ) . '")' ;
		$sql = "SELECT /* SLOW_OK */ /* GLAMOROUS */ gil_wiki,gil_page_title,gil_page_namespace,gil_to FROM commonswiki_p.globalimagelinks where gil_to IN $q" ;
		if ( $ns0 ) $sql .= ' AND gil_page_namespace=""' ;
		
//		print $sql ;

		$res = my_mysql_db_query ( $dbu , $sql , $mysql_con ) ;

		while ( $o = mysql_fetch_object ( $res ) ) {
//			print $o->gil_to . "<br/>" ;
			$a = array () ;
			if ( preg_match ( '/^(.+)(wik.+)$/' , $o->gil_wiki , $a ) ) {
				if ( $a[2] == 'wiki' ) $a[2] = 'wikipedia' ;
				$lp = $a[1] . '.' . $a[2] ;
			} else {
				$lp = '!!!' . $o->gil_wiki ;
			}
			$project_usage[$lp]++ ;
			$usage[$o->gil_to][$lp][$o->gil_page_namespace][$o->gil_page_title] = $o->gil_page_title ;
			$usecount[$o->gil_to]++ ;
			$cnt++ ;
		}
	}

	return $cnt ;
}

function run_globalusage_check ( $category , $depth ) {
	global $ns0 , $show_details , $usage , $usecount , $project_usage ;
	$language = 'commons' ;
	$project = 'wikimedia' ;
	$mysql_con = db_get_con_new ( $language , $project ) ;// , 'userdb' ) ;
#	print '[0]' . mysql_error() . "<br/>" ;
//	print_r ( mysql_get_host_info ( $mysql_con ) ) ; print "\n" ;
//	exit ( 0 ) ;
	$db = get_db_name ( $language , $project ) ;

	$c2 = get_db_safe ( $category ) ;
	if ( $depth > 0 ) {
		$sc = get_subcats ( $category , $depth - 1 ) ;
		$sc[$c2] = $c2 ;
		$c2 = '"' . implode ( '","' , $sc ) . '"' ;
		$c2 = "IN ( $c2 )" ;
	} else {
		$c2 = "=\"$c2\"" ;
	}



	$db = get_db_name ( 'commons' , 'wikimedia' ) ;
	$sql = " SELECT /* SLOW_OK */ /* GLAMOROUS */ gil_wiki,gil_page_title,gil_page_namespace,gil_to from globalimagelinks,page,categorylinks where gil_to=page_title and cl_to $c2 AND page_id=cl_from AND page_namespace=6" ;
	if ( $ns0 ) $sql .= ' AND gil_page_namespace=""' ;
//	print_r ( $sql ) ; print "<br/>" ;
	$res = my_mysql_db_query ( $db , $sql , $mysql_con ) ;

	$cnt = 0 ;
	while ( $o = mysql_fetch_object ( $res ) ) {
		$a = array () ;
		if ( preg_match ( '/^(.+)(wik.+)$/' , $o->gil_wiki , $a ) ) {
			if ( $a[2] == 'wiki' ) $a[2] = 'wikipedia' ;
			$lp = $a[1] . '.' . $a[2] ;
		} else {
			$lp = '!!!' . $o->gil_wiki ;
		}
		$project_usage[$lp]++ ;
		$usage[$o->gil_to][$lp][$o->gil_page_namespace][$o->gil_page_title] = $o->gil_page_title ;
		$usecount[$o->gil_to]++ ;
		$cnt++ ;
	}

	return $cnt ;
}


function to_xml ( $s ) {
	return htmlspecialchars ( $s , ENT_QUOTES ) ;
}

function get_subcats ( $category , $depth ) {
	$done_cats = array () ;
	$sc = db_get_articles_in_category ( 'commons' , $category , $depth , 14 , $done_cats , false , '' , 'wikimedia' , false ) ;
//	print "TESTING : " . count ( $sc ) . "<br/>" ;
//	print_r ( $sc ) ;
	return $sc ;
}

//_________________

$canned = get_request ( 'canned' , '' ) ;
$category = get_request ( 'category' , '' ) ;
if ( $canned != '' ) $category = $canned ;
$wikipedia = get_request ( 'wikipedia' , '' ) ;
$wikisource = get_request ( 'wikisource' , '' ) ;
$wikibooks = get_request ( 'wikibooks' , '' ) ;
$username = get_request ( 'username' , '' ) ;
$gallerypages = get_request ( 'gallerypages' , '' ) ;
$depth = get_request ( 'depth' , 0 ) ;
$ns0 = get_request ( 'ns0' , 0 ) ;
$show_details = get_request ( 'show_details' , 0 ) ;
$show_details_limited = get_request ( 'show_details_limited' , 0 ) ;
if ( $show_details_limited ) $show_details = 1 ;
//$use_globalusage = get_request ( 'use_globalusage' , 0 ) ;
$use_globalusage = 1 ;
$doit = isset ( $_REQUEST['doit'] ) ;

$usage = array () ;
$usecount = array () ;
$project_usage = array () ;

if ( !$doit ) {
	if ( $category == '' and $wikipedia == '' ) $wikipedia = 'de,en,fr,it,pt,ja,es,pl,ru,nl' ;
	$use_globalusage = 1 ;
}

$cb_ns0 = $doit ? ( $ns0 ? 'checked' : '' ) : 'checked' ;
$cb_sd = $doit ? ( $show_details ? 'checked' : '' ) : '' ;
$cb_sdl = $doit ? ( $show_details_limited ? 'checked' : '' ) : '' ;
$cb_gs = $doit ? ( $use_globalusage ? 'checked' : '' ) : 'checked' ;

if ( $format == '' ) {
	print '<html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head><body>' ;
	print get_common_header ( "glamorous.php" , "GLAMorous" ) ;
	
	$popular = array ( "Images from AntWeb" , "Images from Forest & Kim Starr" , "Images from Picswiss" , "Images from Wiki Loves Art Netherlands" , "Images from the Deutsche Fotothek" , "Images from the Geograph British Isles project" , "Images from the Geograph Deutschland project" , "Images from the German Federal Archive" , "Images from the Library of Congress" , "Images from the New York Public Library" , "Images from the Pikiwiki project" , "Images from the Tropenmuseum" , "Kremlin.ru" , "Media from Open Beelden" , "PD-Art (Yorck Project)" , "PD-ScottForesman" , "PD US Air Force" , "PD US Army" , "PD US FEMA" , "PD US Marines" , "PD US Navy" , "Protein structures from PDB" , "Featured pictures on Wikimedia Commons" , "Children's Museum of Indianapolis" ) ;
	$pop = '' ;
	foreach ( $popular AS $p ) {
		$pop .= "<option value=\"$p\">$p</option>" ;
	}
	
	
	print "
	<div><b><big>Also, try the <a href='http://toolserver.org/~magnus/ts2/glamorous'>new version</a>!</big></b></div><br/>
	<form method='post' action='http://toolserver.org/~magnus/glamorous.php'>
	<table border=1>
	<tr><th>Category on Commons</th><td><input type='text' name='category' size='50' value=\"" . str_replace ( '"' , '\"' , $category ) . "\" />
	or <SELECT /* SLOW_OK */ /* GLAMOROUS */ name='canned'><option value=''>--- Popular groups ---</option>$pop</select>
	</td></tr>
	<tr><th>Search depth</th><td><input type='text' name='depth' size='50' value='$depth' /> <small>(0=just this category)</small>
	</td></tr>
	<tr><th>OR user name</th><td><input type='text' name='username' size='50' value='$username' /></td></tr>
	<tr><th>OR gallery pages<br/>(any namespace)</th><td>
	<textarea name='gallerypages' rows='3' cols='80' style='width:100%'>$gallerypages</textarea>
	</td></tr>
	<tr><th/><td>
	<input type='checkbox' name='show_details' value='1' id='show_details' $cb_sd /><label for='show_details'>Show details</label> | 
	<input type='checkbox' name='show_details_limited' value='1' id='show_details_limited' $cb_sdl /><label for='show_details_limited'>Show limited details</label>
	<br/>
	<input type='checkbox' name='ns0' value='1' id='ns0' $cb_ns0 /><label for='ns0'>Main namespace only</label> &nbsp; 
	</td></tr>
	<tr><th/><td><input type='submit' name='doit' value='Do it!' /></td></tr>
	</table>
	</form>
	" ;
}

if ( $doit ) {
	$total = 0 ;
	$img_cnt = get_image_count ( $category , $depth ) ;

	$url = "http://toolserver.org/~magnus/glamorous.php?doit=1&" ;
	if ( $username != '' ) {
		$url .= "username=" . urlencode ( $username ) ;
	} else {
		$url .= "category=" . urlencode ( $category ) ;
	}
	if ( $use_globalusage ) $url .= '&use_globalusage=1' ;
	if ( $ns0 ) $url .= '&ns0=1' ;
	if ( $depth != 0 ) $url .= "&depth=$depth" ;
	if ( $show_details ) $url .= '&show_details=1' ;
	if ( $wikipedia != '' ) $url .= "&wikipedia=$wikipedia" ;
	if ( $wikisource != '' ) $url .= "&wikisource=$wikisource" ;
	if ( $wikibooks != '' ) $url .= "&wikibooks=$wikibooks" ;
	if ( $gallerypages != '' ) $url .= "&gallerypages=" . urlencode ( $gallerypages ) ;
	if ( $show_details_limited ) $url .= "&show_details_limited=1" ;
//	if ( $format != '' ) $url .= "&format=$format" ;
	
	$modename = "category" ;
	if ( $gallerypages != '' ) {
		$category = str_replace ( "\n" , "| " , $gallerypages ) ;
		$modename = "gallery" ;
	}
	if ( $username != '' ) {
		$modename = "user" ;
		$category = $username ;
	}
	
	if ( $format == '' ) {
		print "<p>Copy the URL of <a href='$url'>this link</a> to return to this view. This data is also <a href='$url&format=xml'>available in XML format</a>.</p>" ;
		print "<h2>Results</h2>" ;
		print "<p>" . ucfirst ( $modename ) ." \"$category\" has $img_cnt images.</p>" ;
		print "<table border=1><tr><th>Site</th><th>Images used</th></tr>" ;
	} else if ( $format == 'xml' ) {
		print "<results $modename='".to_xml($category)."' images_in_$modename='$img_cnt' query_url='".to_xml("$url&format=xml")."'>" ;
	}
	
	$xml = '' ;
	
	if ( $gallerypages != '' ) {
		$total = run_globalusage_galleries ( $gallerypages ) ;
		arsort ( $project_usage ) ;
		foreach ( $project_usage AS $project => $count ) {
			if ( $format == '' ) print "<tr><th>$project</th><td>$count</td></tr>" ;
			else if ( $format == 'xml' ) $xml .= "<usage project='$project' usage_count='$count' />" ;
		}
	} else if ( $username != '' ) {
		$total = run_globalusage_username ( $username ) ;
		arsort ( $project_usage ) ;
		foreach ( $project_usage AS $project => $count ) {
			if ( $format == '' ) print "<tr><th>$project</th><td>$count</td></tr>" ;
			else if ( $format == 'xml' ) $xml .= "<usage project='$project' usage_count='$count' />" ;
		}
	} else if ( $use_globalusage ) {
		$total = run_globalusage_check ( $category , $depth ) ;
		arsort ( $project_usage ) ;
		foreach ( $project_usage AS $project => $count ) {
			if ( $format == '' ) print "<tr><th>$project</th><td>$count</td></tr>" ;
			else if ( $format == 'xml' ) $xml .= "<usage project='$project' usage_count='$count' />" ;
		}
	} else {
		$projects = array ( 'wikipedia' , 'wikisource' , 'wikibooks' ) ;
	
		foreach ( $projects AS $project ) {
			$p = $$project ;
			$p = explode ( ',' , $p ) ;
			foreach ( $p AS $w ) {
				$w = trim ( strtolower ( $w ) ) ;
				if ( $w == '' ) continue ;
				$cnt = get_image_usage_count ( $w , 'wikipedia' , $category ) ;
				$total += $cnt ;
				if ( $format == '' ) {
					print "<tr><th>$w.wikipedia</th><td>$cnt</td></tr>" ;
					myflush() ;
				} else if ( $format == 'xml' ) $xml .= "<usage project='wikipedia' usage_count='$cnt' />" ;
			}
		}
	}
	
	if ( $format == '' ) {
		print "<tr><th>Total image usages</th><td>$total</td></tr>" ;
		if ( $show_details or $use_globalusage ) {
			print "<tr><th>Distinct images used</th><td>" . count ( $usage ) ;
			printf ( " (%2.2f%% of all images of $modename)" , 100 * count ( $usage ) / $img_cnt ) ;
			print "</td></tr>" ;
		}
		print "</table>" ;
	} else if ( $format == 'xml' ) {
		print "<stats total_usage='$total'" ;
		if ( $show_details or $use_globalusage ) {
			printf ( " distinct_images='%d'" , count ( $usage ) ) ;
		}
		print ">$xml</stats>" ;
	}
	
	if ( $show_details ) {
		
		arsort ( $usecount ) ;
		
		if ( $format == '' ) {
			print "<h2>Details (top 1000 images)</h2>" ;
			print "<table border=1>" ;
		} else if ( $format == 'xml' ) print "<details>" ;
		
		$top = 0 ;
		foreach ( $usecount AS $image => $image_usage_count ) {
			$top++ ;
			if ( $format == '' and $top > 1000 ) break ;
			$projects = $usage[$image] ;
			$nop = count ( $projects ) ;
			$img_page_url = "http://commons.wikimedia.org/wiki/File:" . myurlencode($image) ;
			$thumb_url = get_thumbnail_url ( 'commons' , $image , 80 , 'wikimedia' ) ;
			
			if ( $format == '' ) {
				$sizefix = '' ;
				if ( substr ( strtolower ( $image ) , -4 , 4 ) == '.ogg' ) $sizefix = " width='80px'" ;
				print "<tr>" ;
				print "<td valign='top' rowspan='$nop'><a target='_blank' href='$img_page_url'><img border='0' src='$thumb_url' $sizefix /></a></td>" ;
				print "<td valign='top' rowspan='$nop'><a target='_blank' href='$img_page_url'>$image</a><br/>used {$image_usage_count}&times;</td>" ;
			} else if ( $format == 'xml' ) {
				print "<image name='".to_xml($image)."' url_page='".to_xml($img_page_url)."' url_thumbnail='".to_xml($thumb_url)."' usage='$image_usage_count'>" ;
			}
			
			$first_row = true ;
			foreach ( $projects AS $project => $ns ) {
				if ( $format == '' ) {
					if ( $first_row ) $first_row = true ;
					else print "<tr>" ;
					print "<td valign='top'>$project</td>" ;
					print "<td valign='top' style='font-size:80%'>" ;
				} else if ( $format == 'xml' ) {
					print "<project name='".to_xml($project)."'>" ;
				}
				
				if ( $show_details_limited ) {
					foreach ( $ns AS $n => $pages ) {
						$nop = count ( $pages ) ;
						if ( $format == 'xml' ) print "<np ns='$n' pages='$nop' />" ;
						else {
							print "<div>" ;
							if ( $n != '' ) print "Namespace $n : " ;
							print "$nop pages</div>" ;
						}
					}
				} else {
					foreach ( $ns AS $n => $pages ) {
						if ( $format == 'xml' ) print "<namespace ns='$n'>" ;
						foreach ( $pages AS $page ) {
							$p = explode ( '.' , $project ) ;
							$pt = $page ;
							if ( $n != '' ) $pt = "$n:$pt" ;
							$purl = get_wikipedia_url ( $p[0] , $pt , '' , $p[1] ) ;
							if ( $format == '' ) {
								if ( !$ns0 ) print "$n : " ;
								print "<a target='_blank' href='$purl'>$page</a><br/>" ;
							} else if ( $format == 'xml' ) {
								print "<page title='".to_xml($page)."' />" ;
							}
						}
						if ( $format == 'xml' ) print "</namespace>" ;
					}
				}
				
				if ( $format == '' ) print "</td></tr>" ;
				else if ( $format == 'xml' ) print "</project>" ;
			}
			
			if ( $format == 'xml' ) print '</image>' ;
		}
		
		if ( $format == '' ) print "</table>" ;
		else if ( $format == 'xml' ) print "</details>" ;
	}
}

if ( $format == '' ) {
	print '</body></html>' ;
} else if ( $format == 'xml' ) {
	print "</results>" ;
}

?>
