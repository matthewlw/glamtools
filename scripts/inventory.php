<?PHP

require_once ( '/data/project/glamtools/public_html/php/ToolforgeCommon.php' ) ;
require_once ( '/data/project/quickstatements/public_html/quickstatements.php' ) ;
//require_once ( '/data/project/glamtools/public_html/php/wikidata.php' ) ;

class Inventory {
	public $tfc , $dbt , $dbc ;
	public $category_depth = 5 ;

	protected $qs ;
	protected $bad_inventory_number_patterns = [
//		'/^LCCN/'
	] ;

	public function __construct () {
		$this->tfc = new ToolforgeCommon ('inventory') ;
		$this->dbt = $this->tfc->openDBtool ( 'inventory_p' ) ;
		$this->dbc = $this->tfc->openDBwiki ( 'commonswiki' ) ;
	}

	protected function normalizeFilename ( $file ) {
		$file = str_replace ( '_' , ' ' , $file ) ;
		return $file ;
	}

	protected function normalizeCommonsCategory ( $commons_category ) {
		$commons_category = preg_replace ( '/^Category:/' , '' , $commons_category ) ; # Paranoia
		return $commons_category ;
	}

	protected function normalizeInventoryNumber ( $inventory_number ) {
		$inventory_number = strip_tags($inventory_number) ;
		$inventory_number = trim($inventory_number) ;
		foreach ( $this->bad_inventory_number_patterns AS $pattern ) {
			if ( preg_match ( $pattern , $inventory_number ) ) return ; // Bad inventory number
		}
		return $inventory_number;
	}

	public function getQS () {
		if ( !isset($this->qs) ) $this->qs = $this->tfc->getQS ( 'glam_inventory' , '/data/project/glamtools/bot.ini' , true ) ;
		return $this->qs ;
	}


	protected function processFile ( $q_institution , $file ) {
		$file = $this->normalizeFilename ( $file ) ;
		$file_dbsafe = $this->dbt->real_escape_string($file) ;
		$sql = "SELECT * FROM `file_inventory` WHERE filename='{$file_dbsafe}'" ;
		$result = $this->tfc->getSQL ( $this->dbt , $sql ) ;
		if ( $o = $result->fetch_object() ) return true ; // We already have that file
		$file_page_url = "https://commons.wikimedia.org/wiki/File:".$this->tfc->urlEncode($file) ;
	#print "{$file_page_url}\n" ;
		$html = @file_get_contents ( $file_page_url ) ;
		if ( !preg_match('|<div class="identifier">\s*(.+?)\s*<|' , $html , $m ) ) return false ; # Get inventory number, return if none
		$inventory_number = $this->normalizeInventoryNumber ( $m[1] ) ;
		if ( !isset($inventory_number) or $inventory_number == '' ) return ;
	
		$q_institution_dbsafe = preg_replace ( '/\D/' , '' , $q_institution ) ;
		$inventory_number_dbsafe = $this->dbt->real_escape_string($inventory_number) ;
		$sql = "INSERT IGNORE INTO `file_inventory` (q_institution,filename,inventory_number) VALUES ({$q_institution_dbsafe},'{$file_dbsafe}','{$inventory_number_dbsafe}')" ;
		$this->tfc->getSQL ( $this->dbt , $sql ) ;
		return true ;
	}

	public function checkInstitution ( $q_institution , $commons_category ) {
		$commons_category = $this->normalizeCommonsCategory ( $commons_category ) ;
		$q_institution_dbsafe = preg_replace ( '/\D/' , '' , $q_institution ) ;
		$q_institution = "Q{$q_institution_dbsafe}" ; # Paranoia

		# Check if this institution has been checked before, and no inventory numbers were found => Don't try again
		$sql = "SELECT * FROM `institution_inventory` WHERE q_institution=$q_institution_dbsafe AND (files_with_inventory_number=0 OR do_ignore=1)" ;
		$result = $this->tfc->getSQL ( $this->dbt , $sql ) ;
		if ( $o = $result->fetch_object() ) return 0 ; // This institution does not have inventory numbers in files

		# Get all files from this category tree
		try {
			$files = array_values ( $this->tfc->getPagesInCategory ( $this->dbc , $commons_category , $this->category_depth , 6 , true ) ) ;
		} catch ( Exception $e ) {
			echo 'Caught exception from category SQL: ',  $e->getMessage(), "\n";
			return ;
		}

		# Process files and get inventory numbers
		$file_with_inventory_number = 0 ;
		foreach ( $files AS $file ) {
			$has_inventory_number = $this->processFile ( $q_institution , $file ) ;
			if ( $has_inventory_number ) $file_with_inventory_number++ ;
		}
		$sql = "REPLACE INTO `institution_inventory` (q_institution,files_with_inventory_number) VALUES ({$q_institution_dbsafe},{$file_with_inventory_number})" ;
		$this->tfc->getSQL ( $this->dbt , $sql ) ;
		return $file_with_inventory_number ;
	}

	public function scanAll () {
		$sparql = 'SELECT ?q ?cat { ?q wdt:P31|wdt:P279* wd:Q33506 ; wdt:P373 ?cat }' ;
		$j = $this->tfc->getSPARQL ( $sparql ) ; # Get museums with Commons category
		shuffle ( $j->results->bindings ) ; # For variety...
		foreach ( $j->results->bindings AS $b ) {
			$q_institution = $this->tfc->parseItemFromURL ( $b->q->value ) ;
			$commons_category = $b->cat->value ;
			$this->checkInstitution ( $q_institution , $commons_category ) ;
		}
	}

	public function matchFilesForInstitution ( $q_institution ) {
		$q_institution_dbsafe = preg_replace ( '/\D/' , '' , $q_institution ) ;
		$q_institution = "Q{$q_institution_dbsafe}" ; # Paranoia

		# Check items missing files for this institution
		$inventory_number2q = [] ;
		$sparql = "SELECT ?q ?in { ?q wdt:P195 wd:{$q_institution} ; wdt:P217 ?in MINUS { ?q wdt:P18 [] } }" ;
		$j = $this->tfc->getSPARQL ( $sparql ) ;
		foreach ( $j->results->bindings AS $b ) {
			$inventory_number2q[$b->in->value] = $this->tfc->parseItemFromURL ( $b->q->value ) ;
		}

		# Check files/inventory numbers
		$commands = [] ;
		$sql = "SELECT * FROM `file_inventory` WHERE `q_institution`={$q_institution_dbsafe} AND `done`=0" ;
		$result = $this->tfc->getSQL ( $this->dbt , $sql ) ;
		while ( $o = $result->fetch_object() ) {
			if ( isset($inventory_number2q[$o->inventory_number]) ) {
				$q = $inventory_number2q[$o->inventory_number] ;
				$filename = $this->normalizeFilename ( $o->filename ) ; # Paranoia
				$commands[] = "{$q}\tP18\t\"{$filename}\"" ;
				$sql = "UPDATE `file_inventory` SET `done`=1 WHERE `id`={$o->id}" ;
				$this->tfc->getSQL ( $this->dbt , $sql ) ;
			} else {
#				print "* No item for inventory number {$o->inventory_number}, file {$o->filename}\n" ;
			}
		}

		if ( count($commands) == 0 ) return ;
		print_r ( $commands ) ;
		$this->tfc->runCommandsQS ( $commands , $this->getQS() ) ;
	}

	public function matchAll () {
		$sql = "SELECT distinct q_institution FROM file_inventory WHERE done=0" ;
		$result = $this->tfc->getSQL ( $this->dbt , $sql ) ;
		while ( $o = $result->fetch_object() ) {
			$q_institution = 'Q' . $o->q_institution ;
			$this->matchFilesForInstitution ( $q_institution ) ;
		}
	}

}

?>
